<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class articulo extends Controller
{
	 public function index()
    {
        $marca = DB::table('marca')->get();
        $proveedor = DB::table('proveedor')->get();
        $estilo = DB::table('estilo')->get();
        $concepto = DB::table('concepto')->get();
		$tallas = DB::table('tallas')->get();
		$linea = DB::table('linea')->get();
		$temporada = DB::table('temporada')->get();
		$acabado = DB::table('acabado')->get();
		$color = DB::table('color')->get();

       	 $datos = array(
	        'marca' => $marca,
	        'proveedor'	=>	$proveedor,
          	'estilo'	=>	$estilo,
            'concepto'	=>	$concepto,
			'tallas'	=>	$tallas,
			'linea'	=>	$linea,
			'temporada'	=>	$temporada,
			'acabado'	=>	$acabado,
			'color'	=>	$color);

      	  return response()->json($datos, 200);
    } 
    
    function nuevoArticulo(Request $request){
    	
    	echo 123;

    }	   
}
