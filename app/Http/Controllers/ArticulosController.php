<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ArticulosController  extends Controller
{
	 public function index()
    {
        $marca = DB::table('marca')->get();
        $proveedor = DB::table('proveedor')->get();
        $estilo = DB::table('estilo')->get();
        $concepto = DB::table('concepto')->get();
		$tallas = DB::table('tallas')->get();
		$linea = DB::table('linea')->get();
		$temporada = DB::table('temporada')->get();
		$acabado = DB::table('acabado')->get();
		$color = DB::table('color')->get();

       	 $datos = array(
	        'marca' 	=>	$marca,
	        'proveedor'	=>	$proveedor, 
          	'estilo'	=>	$estilo,
            'concepto'	=>	$concepto,
			'tallas'	=>	$tallas,
			'linea'		=>	$linea,
			'temporada'	=>	$temporada,
			'acabado'	=>	$acabado,
			'color'		=>	$color);

      	  return response()->json($datos, 200);
    } 
    
    function nuevoArticulo(Request $request){
    	
    	return response()->json($request->all());

	}	

	function validar(Request $datos_a_insertar){

		DB::table('marca')->insert($datos_a_insertar);	
		DB::table('proveedor')->insert($datos_a_insertar);	
		DB::table('estilo')->insert($datos_a_insertar);	
		DB::table('concepto')->insert($datos_a_insertar);	
		DB::table('tallas')->insert($datos_a_insertar);	
		DB::table('linea')->insert($datos_a_insertar);	
		DB::table('temporada')->insert($datos_a_insertar);	
		DB::table('acabado')->insert($datos_a_insertar);	
		DB::table('color')->insert($datos_a_insertar);
			
		
		$datos_a_insertar = array(
	        'marca' 	=>	$marca,
	        'proveedor'	=>	$proveedor, 
          	'estilo'	=>	$estilo,
            'concepto'	=>	$concepto,
			'tallas'	=>	$tallas,
			'linea'		=>	$linea,
			'temporada'	=>	$temporada,
			'acabado'	=>	$acabado,
			'color'		=>	$color);	
		
		  return response()->json($datos_a_insertar->all());
	}	  
}
